export const colors = {
  backgroundLight: '#F9F9F9',
  backgroundDark: '#313131',
  lightPink: '#D9A3AC',
  darkPink: '#A62145',
  lightGray: '#C4C4C4',
  black: '#000',
  white: '#FFF',
  darkGray: '#3A3A3A',
};
