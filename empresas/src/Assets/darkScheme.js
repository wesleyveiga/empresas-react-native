import {colors} from './colors';

export const darkScheme = {
  compContainer: {
    container: {
      flex: 1,
      backgroundColor: colors.backgroundDark,
    },
  },
  compInput: {
    container: {
      height: 40,
      borderBottomWidth: 1,
      marginHorizontal: '5%',
      borderRadius: 10,
      paddingHorizontal: '2%',
      marginVertical: '2%',
    },
  },
  login: {
    icon: {
      width: '80%',
      alignSelf: 'center',
      resizeMode: 'contain',
      marginTop: '30%',
      tintColor: colors.black,
    },
    titleText: {
      marginTop: '10%',
      fontWeight: 'bold',
      fontSize: 30,
      textAlign: 'center',
      color: '#000',
    },
  },
  cardEnterprise: {
    container: {
      backgroundColor: colors.darkGray,
      width: '95%',
      borderRadius: 15,
      alignSelf: 'center',
      marginVertical: 5,
      minHeight: 100,
      paddingHorizontal: '3%',
      paddingVertical: '3%',
      flexDirection: 'row',
      shadowColor: '#000',
      shadowOffset: {
        width: 0,
        height: 2,
      },
      shadowOpacity: 0.25,
      shadowRadius: 3.84,

      elevation: 5,
    },
    textContainer: {
      borderRightWidth: 0.5,
      borderColor: colors.lightGray,
      flex: 1,
      padding: '1%',
    },
    imageContainer: {
      flex: 1,
      padding: '1%',
    },
    title: {
      textAlign: 'center',
      fontWeight: 'bold',
      fontSize: 15,
      color: colors.lightGray,
    },
    text: {
      paddingTop: '5%',
      textAlign: 'justify',
      color: colors.lightGray,
    },
    image: {
      width: '95%',
      height: 120,
      borderRadius: 15,
      alignSelf: 'center',
    },
  },
  detailEnterprise: {
    title: {
      fontWeight: 'bold',
      fontSize: 15,
      color: colors.lightGray,
    },
    text: {
      paddingTop: '5%',
      textAlign: 'justify',
      color: colors.lightGray,
    },
    image: {
      width: '95%',
      height: 250,
      borderRadius: 15,
      alignSelf: 'center',
    },
    scrollView: {
      margin: '5%',
      flex: 1,
    },
  },
};
