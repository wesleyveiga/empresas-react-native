import axios from 'axios';

export const API = (access_token, client, uid) =>
  axios.create({
    baseURL: 'https://empresas.ioasys.com.br/api/v1/',
    headers: {
      'Content-type': 'application/json',
      'access-token': access_token,
      client: client,
      uid: uid,
    },
    timeout: 1000,
  });
