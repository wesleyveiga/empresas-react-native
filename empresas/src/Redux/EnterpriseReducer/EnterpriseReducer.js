import {API} from '../../Configs/AxiosConfigs';
import {handleApi, handleApiSuccess, handleApiError} from '../ActionConfigs';

//TYPES
const Types = {
  ENTERPRISES: 'enterprise/ENTERPRISES',
};

const payload = {
  response: null,
  error: null,
  isLoading: false,
};

//REDUCERS
const INITIAL_STATE = {
  enterprises: payload,
};

export const reducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.ENTERPRISES:
      return {
        ...state,
        enterprises: action.payload,
      };
    default:
      return state;
  }
};

//ACTIONS
export const enterprises = () => {
  console.log('fui chamado');
  return async (dispatch, getState) => {
    const accessToken =
      getState().UserReducer.login_data.response.headers['access-token'];
    const client = getState().UserReducer.login_data.response.headers.client;
    const uid = getState().UserReducer.login_data.response.headers.uid;

    try {
      handleApi(true, Types.ENTERPRISES, dispatch);
      API(accessToken, client, uid)
        .get('enterprises')
        .then(res => {
          console.log('res', res);
          handleApiSuccess(res.data.enterprises, Types.ENTERPRISES, dispatch);
        })
        .catch(err => {
          console.log('err: ', err);
          handleApiError(err, Types.ENTERPRISES, dispatch);
        });
    } catch (ex) {
      handleApiError(ex, Types.ENTERPRISES, dispatch);
      throw ex;
    }
  };
};
