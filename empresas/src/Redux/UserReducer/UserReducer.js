import {Alert} from 'react-native';
import {API} from '../../Configs/AxiosConfigs';
import {handleApi, handleApiSuccess, handleApiError} from '../ActionConfigs';

//TYPES
const Types = {
  LOGGED: 'user/LOGGED',
  LOGIN_DATA: 'user/LOGIN_DATA',
  LOGOFF: 'user/LOGOFF',
};

const payload = {
  response: null,
  error: null,
  isLoading: false,
};

//REDUCERS
const INITIAL_STATE = {
  logged: false,
  login_data: payload,
};

export const reducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.LOGGED:
      return {
        ...state,
        logged: action.logged,
      };
    case Types.LOGIN_DATA:
      return {
        ...state,
        login_data: action.payload,
      };
    case Types.LOGOFF:
      return {
        ...state,
        logged: false,
        login_data: {
          isLoading: false,
          response: null,
          error: null,
          headers: [],
        },
      };
    default:
      return state;
  }
};

//ACTIONS
export const login = user => {
  return async dispatch => {
    try {
      handleApi(true, Types.LOGIN_DATA, dispatch);
      API()
        .post('users/auth/sign_in', user)
        .then(res => {
          console.log('res', res);
          handleApiSuccess(res, Types.LOGIN_DATA, dispatch);
          dispatch({
            type: Types.LOGGED,
            logged: true,
          });
        })
        .catch(err => {
          console.log('err: ', err.response.data.status);
          err.response.status == 401 &&
            Alert.alert(
              'Atenção',
              'Usuário ou senha incorretos.',
              [{text: 'OK'}],
              {
                cancelable: true,
              },
            );
          handleApiError(err, Types.LOGIN_DATA, dispatch);
        });
    } catch (ex) {
      handleApiError(ex, Types.LOGIN_DATA, dispatch);
      throw ex;
    }
  };
};

export const logoff = () => {
  return async dispatch => {
    try {
      dispatch({
        type: Types.LOGOFF,
      });
    } catch (ex) {
      throw ex;
    }
  };
};
