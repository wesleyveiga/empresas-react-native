//TYPES
const Types = {
  SET_DARK_MODE: 'app/SET_DARK_MODE',
};

//REDUCERS
const INITIAL_STATE = {
  isDarkMode: false,
};

export const reducers = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case Types.SET_DARK_MODE:
      return {
        ...state,
        isDarkMode: action.value,
      };
    default:
      return state;
  }
};

//ACTIONS
export const setDarkMode = darkMode => {
  console.log(darkMode);
  return async dispatch => {
    dispatch({
      type: Types.SET_DARK_MODE,
      value: darkMode,
    });
  };
};
