import {combineReducers} from 'redux';
import {AppReducer} from './AppReducer';
import {UserReducer} from './UserReducer';
import {EnterpriseReducer} from './EnterpriseReducer';

const appReducer = combineReducers({
  AppReducer,
  UserReducer,
  EnterpriseReducer,
});

const rootReducer = (state, action) => {
  return appReducer(state, action);
};

export default rootReducer;
