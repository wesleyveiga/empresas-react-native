import React, {useEffect} from 'react';
import {View, Text, FlatList} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {enterprises} from '../../Redux/EnterpriseReducer/EnterpriseReducer';
import Container from '../../Components/Container';
import Header from '../../Components/Header';
import CardEnterprise from '../../Components/CardEnterprise/CardEnterprise';
import {logoff} from '../../Redux/UserReducer/UserReducer';

const Enterprises = ({navigation}) => {
  const dispatch = useDispatch();
  const enterprises_data = useSelector(
    store => store.EnterpriseReducer.enterprises,
  );

  useEffect(() => {
    dispatch(enterprises());
  }, []);

  // ;

  const _renderCard = ({item}) => {
    return (
      <CardEnterprise
        city={item.city}
        country={item.country}
        description={item.description}
        email={item.email_enterprise}
        name={item.enterprise_name}
        type={item.enterprise_type}
        facebook={item.facebook}
        linkedin={item.linkedin}
        phone={item.phone}
        photo={item.photo}
        sharePrice={item.sharePrice}
        twitter={item.twitter}
        value={item.value}
        share_price={item.share_price}
        onPress={() => navigation.navigate('EnterpriseDetail', {item: item})}
      />
    );
  };

  const exit = () => {
    dispatch(logoff());
  };

  return (
    <Container>
      <Header title={'Empresas'} onPress={() => exit()} />
      <FlatList
        data={enterprises_data.response}
        renderItem={_renderCard}
        keyExtractor={item => item.id}
        refreshing={enterprises_data.isLoading}
        onRefresh={() => dispatch(enterprises())}
      />
    </Container>
  );
};

export default Enterprises;
