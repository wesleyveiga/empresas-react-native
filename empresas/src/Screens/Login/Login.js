import React, {useState} from 'react';
import {
  View,
  Text,
  useColorScheme,
  Image,
  TextInput,
  TouchableOpacity,
  BackgroundImage,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {login} from '../../Redux/UserReducer/UserReducer';
import Container from '../../Components/Container';
import Input from '../../Components/Input';
import Button from '../../Components/Buttom';
import {lightScheme} from '../../Assets/lightScheme';
import {darkScheme} from '../../Assets/darkScheme';
import {colors} from '../../Assets/colors';

const Login = ({navigation}) => {
  const dispatch = useDispatch();

  const isDarkMode = useSelector(store => store.AppReducer.isDarkMode);
  const loading = useSelector(store => store.UserReducer.login_data.isLoading);

  const teste = useSelector(store => store.UserReducer.login_data);
  console.log(teste);

  const light = lightScheme.login;
  const dark = darkScheme.login;

  const [user, setUser] = useState({
    email: '',
    password: '',
  });

  return (
    <Container>
      <View style={{flex: 5}}>
        <Image
          source={require('../../Assets/Icons/login.png')}
          style={{position: 'absolute', marginTop: -50}}
          tintColor={isDarkMode ? colors.darkPink : colors.lightPink}
        />
        <Image
          tintColor={isDarkMode ? colors.lightGray : colors.black}
          style={isDarkMode ? dark.icon : light.icon}
          source={require('../../Assets/Icons/logo_ioasys.png')}
        />
      </View>
      <View style={{flex: 3}}>
        <Input
          title="Usuário"
          placeholder="usuario@email.com.br"
          onChangeText={text => setUser({...user, email: text})}
          autoCapitalize={'none'}
        />
        <Input
          title="Senha"
          placeholder="********"
          secureTextEntry
          onChangeText={text => setUser({...user, password: text})}
          autoCapitalize={'none'}
        />
      </View>
      <View
        style={{
          flex: 1,
          alignItems: 'center',
        }}>
        <Button
          title="Acessar"
          onPress={() => dispatch(login(user))}
          isLoading={loading}
        />
      </View>
    </Container>
  );
};

export default Login;
