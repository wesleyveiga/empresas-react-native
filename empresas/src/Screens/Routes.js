import React, {useEffect} from 'react';
import {useColorScheme} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {setDarkMode} from '../Redux/AppReducer/AppReducer';
//Screens
import Login from './Login';
import Enterprises from './Enterprises';
import EnterpriseDetail from './EnterpriseDetail/EnterpriseDetail';

const Routes = () => {
  const logged = useSelector(store => store.UserReducer.logged);
  const Stack = createStackNavigator();

  const dispatch = useDispatch();
  const colorScheme = useColorScheme();

  useEffect(() => {
    dispatch(setDarkMode(colorScheme === 'dark'));
  }, [useColorScheme()]);

  return !logged ? (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Login" component={Login} />
      </Stack.Navigator>
    </NavigationContainer>
  ) : (
    <NavigationContainer>
      <Stack.Navigator
        initialRouteName="Login"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Enterprises" component={Enterprises} />
        <Stack.Screen name="EnterpriseDetail" component={EnterpriseDetail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default Routes;
