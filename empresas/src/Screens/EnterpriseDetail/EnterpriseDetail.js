import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {useSelector} from 'react-redux';
import Container from '../../Components/Container';
import Header from '../../Components/Header';
import {API} from '../../Configs/Constants';
import {lightScheme} from '../../Assets/lightScheme';
import {darkScheme} from '../../Assets/darkScheme';

const EnterpriseDetail = ({route, navigation}) => {
  const {item} = route.params;

  const isDarkMode = useSelector(store => store.AppReducer.isDarkMode);
  const light = lightScheme.detailEnterprise;
  const dark = darkScheme.detailEnterprise;

  return (
    <Container>
      <Header
        title={item.enterprise_name}
        back
        onPress={() => navigation.goBack()}
      />
      <Image
        source={{uri: API + item.photo}}
        style={light.image}
        resizeMethod={'resize'}
      />
      <ScrollView style={light.scrollView}>
        <Text style={isDarkMode ? dark.title : light.title}>Descrição</Text>
        <Text style={isDarkMode ? dark.text : light.text}>
          {item.description}
        </Text>
        <Text style={isDarkMode ? dark.text : light.text}>
          <Text style={{fontWeight: 'bold'}}>Preço do compartilhamento: </Text>
          USD: {item.share_price.toFixed(2)}
        </Text>
        <Text style={isDarkMode ? dark.text : light.text}>
          <Text style={{fontWeight: 'bold'}}>Cidade: </Text>
          {item.city}
        </Text>
        <Text style={isDarkMode ? dark.text : light.text}>
          <Text style={{fontWeight: 'bold'}}>Estado: </Text>
          {item.country}
        </Text>
        <Text style={isDarkMode ? dark.text : light.text}>
          <Text style={{fontWeight: 'bold'}}>Tipo da empresa:</Text>
          {item.enterprise_type.enterprise_type_name}
        </Text>
      </ScrollView>
      <View
        style={{
          flexDirection: 'row',
          width: '90%',
          justifyContent: 'space-between',
          alignSelf: 'center',
          marginVertical: '5%',
        }}>
        <TouchableOpacity
          disabled={item.email_enterprise ? false : true}
          onPress={() => Linking(item.email_enterprise)}>
          <Image
            source={require('../../Assets/Icons/email.png')}
            style={{width: 40, height: 40}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          disabled={item.facebook ? false : true}
          onPress={() => Linking(item.facebook)}>
          <Image
            source={require('../../Assets/Icons/facebook.png')}
            style={{width: 40, height: 40}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          disabled={item.twitter ? false : true}
          onPress={() => Linking(item.twitter)}>
          <Image
            source={require('../../Assets/Icons/twitter.png')}
            style={{width: 40, height: 40}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          disabled={item.linkedin ? false : true}
          onPress={() => Linking(item.linkedin)}>
          <Image
            source={require('../../Assets/Icons/linkedin.png')}
            style={{width: 40, height: 40}}
          />
        </TouchableOpacity>
        <TouchableOpacity
          disabled={item.phone ? false : true}
          onPress={() => Linking(item.phone)}>
          <Image
            source={require('../../Assets/Icons/phone.png')}
            style={{
              width: 40,
              height: 40,
            }}
          />
        </TouchableOpacity>
      </View>
    </Container>
  );
};

export default EnterpriseDetail;
