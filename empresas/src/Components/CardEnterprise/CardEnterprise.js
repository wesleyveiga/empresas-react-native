import React from 'react';
import {View, Text, Image, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';
import {API} from '../../Configs/Constants';
import {lightScheme} from '../../Assets/lightScheme';
import {darkScheme} from '../../Assets/darkScheme';

const CardEnterprise = props => {
  const isDarkMode = useSelector(store => store.AppReducer.isDarkMode);
  const light = lightScheme.cardEnterprise;
  const dark = darkScheme.cardEnterprise;

  return (
    <TouchableOpacity
      style={isDarkMode ? dark.container : light.container}
      onPress={() => props.onPress()}>
      <View style={isDarkMode ? dark.textContainer : light.textContainer}>
        <Text style={isDarkMode ? dark.title : light.title}>{props.name}</Text>
        <Text style={isDarkMode ? dark.text : light.text}>
          {props.description.length > 300
            ? props.description.slice(0, 300) + '...'
            : props.description}
        </Text>
      </View>
      <View style={isDarkMode ? dark.imageContainer : light.imageContainer}>
        <Image
          source={{uri: API + props.photo}}
          style={light.image}
          resizeMethod={'resize'}
        />
        <Text
          style={[
            isDarkMode ? dark.text : light.text,
            {textAlign: 'center', fontWeight: 'bold'},
          ]}>
          Localização
        </Text>
        <Text
          style={[isDarkMode ? dark.text : light.text, {textAlign: 'center'}]}>
          {props.city}, {props.country}
        </Text>
        <Text
          style={[
            isDarkMode ? dark.text : light.text,
            {textAlign: 'center', fontWeight: 'bold'},
          ]}>
          Preço de compartilhamento
        </Text>
        <Text
          style={[isDarkMode ? dark.text : light.text, {textAlign: 'center'}]}>
          USD: {props.share_price.toFixed(2)}
        </Text>
        <Text
          style={[
            isDarkMode ? dark.text : light.text,
            {textAlign: 'center', fontWeight: 'bold'},
          ]}>
          Tipo da empresa
        </Text>
        <Text
          style={[isDarkMode ? dark.text : light.text, {textAlign: 'center'}]}>
          {props.type.enterprise_type_name}
        </Text>
      </View>
    </TouchableOpacity>
  );
};

export default CardEnterprise;
