import React, {Fragment} from 'react';
import {View, Text, StyleSheet, Image, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';

const Header = props => {
  const isDarkMode = useSelector(store => store.AppReducer.isDarkMode);
  return (
    <View style={styles.container}>
      <Image
        source={require('../../Assets/Icons/login.png')}
        style={styles.image}
        width={'110%'}
      />
      <View style={{flexDirection: 'row', height: 120, flex: 1}}>
        {props.back ? (
          <TouchableOpacity
            style={{justifyContent: 'center'}}
            onPress={() => props.onPress()}>
            <Image
              source={require('../../Assets/Icons/back.png')}
              style={{
                width: 50,
                height: 40,
                marginLeft: '7%',
                tintColor: isDarkMode ? '#C4C4C4' : '#000',
              }}
            />
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={{justifyContent: 'center'}}
            onPress={() => props.onPress()}>
            <Image
              source={require('../../Assets/Icons/logout.png')}
              style={{
                width: 40,
                height: 40,
                marginLeft: '7%',
                tintColor: isDarkMode ? '#C4C4C4' : '#000',
              }}
            />
          </TouchableOpacity>
        )}
        <View
          style={{
            alignSelf: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            flex: 1,
          }}>
          <Text style={[styles.title(isDarkMode), props.style]}>
            {props.title}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default Header;

const styles = StyleSheet.create({
  container: {
    height: 120,
  },
  title: isDark => ({
    fontSize: 32,
    textAlign: 'center',
    paddingTop: '3%',
    color: isDark ? '#C4C4C4' : '#000',
  }),
  image: {
    position: 'absolute',
    marginTop: -330,
  },
});
