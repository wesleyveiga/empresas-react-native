import React from 'react';
import {SafeAreaView} from 'react-native';
import {useSelector} from 'react-redux';
import {lightScheme} from '../../Assets/lightScheme';
import {darkScheme} from '../../Assets/darkScheme';

const Container = props => {
  const isDarkMode = useSelector(store => store.AppReducer.isDarkMode);
  const light = lightScheme.compContainer;
  const dark = darkScheme.compContainer;

  return (
    <SafeAreaView style={isDarkMode ? dark.container : light.container}>
      {props.children}
    </SafeAreaView>
  );
};

export default Container;
