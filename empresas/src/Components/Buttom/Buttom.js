import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  ActivityIndicator,
} from 'react-native';
import {useSelector} from 'react-redux';
import {colors} from '../../Assets/colors';
import {shader} from '../../Assets/shader';

const Buttom = props => {
  const isDarkMode = useSelector(store => store.AppReducer.isDarkMode);

  return (
    <TouchableOpacity
      style={[
        styles.container(isDarkMode, shader),
        props.style ? props.style : {},
      ]}
      onPress={props.onPress}>
      {props.isLoading ? (
        <ActivityIndicator color="#FFF" size="large" />
      ) : (
        <Text style={[styles.title, props.textStyle ? props.textStyle : {}]}>
          {props.title}
        </Text>
      )}
    </TouchableOpacity>
  );
};

export default Buttom;

const styles = StyleSheet.create({
  container: (isDarkMode, shader) => ({
    width: 186,
    height: 60,
    backgroundColor: isDarkMode ? colors.darkPink : colors.lightPink,
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
    shader,
  }),
  title: {
    fontSize: 24,
    color: 'white',
  },
});
