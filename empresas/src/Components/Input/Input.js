import React, {Fragment} from 'react';
import {TextInput, Text, StatusBar} from 'react-native';
import {useSelector} from 'react-redux';
import {lightScheme} from '../../Assets/lightScheme';
import {darkScheme} from '../../Assets/darkScheme';
import {colors} from '../../Assets/colors';

const Input = props => {
  const isDarkMode = useSelector(store => store.AppReducer.isDarkMode);
  const light = lightScheme.compInput;
  const dark = darkScheme.compInput;

  return (
    <Fragment>
      <StatusBar
        backgroundColor={isDarkMode ? colors.darkPink : colors.lightPink}
        barStyle={isDarkMode ? 'light-content' : 'dark-content'}
      />
      <Text
        style={{fontSize: 18, paddingHorizontal: '2%', marginHorizontal: '5%'}}>
        {props.title}
      </Text>
      <TextInput
        style={[isDarkMode ? dark.container : light.container, props.style]}
        placeholder={props.placeholder}
        placeholderTextColor={isDarkMode ? colors.darkGray : colors.lightGray}
        value={props.value}
        onChangeText={props.onChangeText}
        keyboardType={props.keyboardType}
        autoCapitalize={props.autoCapitalize}
        autoCompleteType={props.autoCompleteType}
        keyboardAppearance={isDarkMode ? 'light' : 'dark'}
        secureTextEntry={props.secureTextEntry}
      />
    </Fragment>
  );
};

export default Input;
